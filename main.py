import asyncio
import logging
import os
import time
import httpx
from dotenv import load_dotenv, find_dotenv
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from src.db.base import Base
from src.constants.config import MAIN_URL
from src.parsing.parse_statement import parse_result, save_results

load_dotenv(find_dotenv())


async def fetch_item_details(item_num: str, main_url: str):
    get_params = {
        'action': 'wbgetentities',
        'format': 'json',
        'ids': item_num
    }

    async with httpx.AsyncClient() as client:
        response = await client.get(main_url, params=get_params)
        return {'item_num': item_num, 'data': response.json()}


async def main():
    start_time = time.time()
    engine = create_async_engine(os.getenv("DB_URL"), connect_args={'timeout': 10})
    db_pool = async_sessionmaker(engine, expire_on_commit=False)
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    item_nums = os.getenv("ITEMS_IDS").split(',')
    main_url = MAIN_URL
    responses = await asyncio.gather(*(fetch_item_details(item, main_url) for item in item_nums))
    results = await asyncio.gather(*(parse_result(result, db_pool) for result in responses))

    save_to_db_tasks = []
    for parsed in results:
        save_to_db_tasks.append(asyncio.create_task(save_results(db_pool, parsed)))

    await asyncio.gather(*save_to_db_tasks)

    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Elapsed time:", elapsed_time, "seconds")


if __name__ == "__main__":
    asyncio.run(main())
