requests==2.31.0
bs4==0.0.2
lxml==5.1.0
sqlalchemy==2.0.28
httpx==0.27.0
aiosqlite==0.20.0
python-dotenv==1.0.1
