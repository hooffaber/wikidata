import os
from typing import List
from string import ascii_lowercase
from time import sleep
import requests

cookies = {}

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://www.techopedia.com/dictionary',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest',
    'Origin': 'https://www.techopedia.com',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
}

LETTER_OFFSET_STEP = 15
MAIN_URL = 'https://www.techopedia.com/wp-admin/admin-ajax.php'
COOKIES = cookies
HEADERS = headers
LAST_LETTER = 'l'


def get_en_alphabet() -> List[str]:
    """
    :return: ['a', 'b', 'c', ..., 'z']
    """
    return list(ascii_lowercase)


def get_words_by_letter(letter: str) -> None:
    """
    Get's .html data just for one letter - saved in /data/
    :param letter:
    :return: None
    """
    offset_start = 0

    goNextLetter = False
    offset = offset_start

    while not goNextLetter:
        words_part = get_next_words_part(letter, offset)
        if words_part == 'stop':
            goNextLetter = True
        elif words_part:
            raise RuntimeError(f'Ошибка на букве {letter} с offset = {words_part}')
        else:
            offset += LETTER_OFFSET_STEP


def get_next_words_part(letter: str, offset: int) -> int | str:
    """
    Gets .html data for letter for current offset. Offset is field in data, that points to what
        portion of words site should load.
    :param letter:
    :param offset:
    :return: 0 - if data was parsed correctly,
    :return: offset - if data wasn't parsed, because of site security service
    :return: string "stop" if there's no offset like in param (response text is empty)
    """
    print(f'[X][X] Обработка смещения {offset} буквы {letter}')
    data = {
        'action': 'aplha_term_loadMore',
        'dataoffset': f'{offset}',
        'dataletter': f'{letter}',
    }

    response = requests.post(MAIN_URL, cookies=COOKIES, headers=HEADERS, data=data)

    if response.status_code == 200:
        sleep(2)
        if response.text == '0':
            return 'stop'
        save_letter_data_html(letter, offset, response.text)
    elif response.status_code == 403:
        return offset

    return 0


def get_words():
    alphabet = get_en_alphabet()

    last_letter = LAST_LETTER

    alphabet = alphabet[alphabet.index(last_letter):]

    for letter in alphabet:
        print(f'[X] Обработка буквы {letter}:')
        get_words_by_letter(letter)
        sleep(5)

    print(f'[X] Все буквы обработаны!')


def save_letter_data_html(letter: str, offset: int, text: str):
    """
    Saves raw .html data, that was responsed from server, by it's letter and offset
    :param letter:
    :param offset:
    :param text:
    :return:
    """
    if not os.path.exists(f'data/letters/{letter}'):
        os.makedirs(f'data/letters/{letter}')

    with open(f'data/letters/{letter}/{letter}_{offset}.html', 'w', encoding='utf-8') as file:
        file.write(text)


if __name__ == '__main__':
    get_words()
