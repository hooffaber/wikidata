import os
import json
from typing import List
from bs4 import BeautifulSoup
from techdictionary import get_en_alphabet


def check_html(html_path: str):
    with open(html_path, 'r', encoding='utf-8') as file:
        bs = BeautifulSoup(file, 'lxml')
    words = []
    li_s = bs.findAll('li')
    for li in li_s:
        words.append(li.text)

    return words


def check_letter(folder_path: str):
    files = os.listdir(folder_path)
    words = []
    for file in files:
        file_fullpath = os.path.join(folder_path, file)
        words.extend(check_html(file_fullpath))

    return words


def check_data():
    letters = get_en_alphabet()
    result_data = {}
    for letter in letters:
        letter_folder = f'data/letters/{letter}'
        result_data[letter] = check_letter(letter_folder)

    return result_data


if __name__ == '__main__':
    data = check_data()

    with open('data/techdict.json', 'w', encoding='utf-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)



