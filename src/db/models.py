from typing import Optional

from sqlalchemy import String, ForeignKey, create_engine
from sqlalchemy.orm import mapped_column, Mapped, relationship

from src.db.base import Base


class Item(Base):
    __tablename__ = "items"

    wikidata_id: Mapped[str] = mapped_column(String, primary_key=True, unique=True)
    statements: Mapped[list["Statement"]] = relationship(back_populates="items",
                                                         secondary='item_statements',
                                                         lazy='selectin')
    site_links: Mapped[list["SiteLink"]] = relationship(back_populates='item', lazy='selectin')
    labels: Mapped[list["Label"]] = relationship(back_populates='item', lazy='selectin')

    def __repr__(self):
        return f'{self.wikidata_id=}'


class Property(Base):
    __tablename__ = 'properties'

    wikidata_id: Mapped[str] = mapped_column(String, primary_key=True, unique=True)
    title: Mapped[Optional[str]]
    datatype: Mapped[Optional[str]] = mapped_column(String, nullable=False)


class Statement(Base):
    __tablename__ = 'statements'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    property_id: Mapped[int] = mapped_column(ForeignKey('properties.wikidata_id'))
    value: Mapped[str] = mapped_column(nullable=False)
    rank: Mapped[str] = mapped_column(String, nullable=False)

    items: Mapped[list["Item"]] = relationship(back_populates="statements", secondary='item_statements', lazy='selectin')
    qualifiers: Mapped[list["Qualifier"]] = relationship(back_populates="statement", lazy='selectin')
    references: Mapped[list["Reference"]] = relationship(back_populates="statement", lazy='selectin')

    def __repr__(self):
        return f'{self.property_id}={self.value}'


class SiteLink(Base):
    __tablename__ = 'sitelinks'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    wikidata_id: Mapped[str] = mapped_column(ForeignKey('items.wikidata_id'))
    site: Mapped[str]
    title: Mapped[str]
    badges: Mapped[Optional[str]]

    item: Mapped["Item"] = relationship(back_populates='site_links')


class Reference(Base):
    __tablename__ = 'references'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    inside_id: Mapped[Optional[int]]
    statement_id: Mapped[int] = mapped_column(ForeignKey('statements.id'))

    property_id: Mapped[int] = mapped_column(ForeignKey('properties.wikidata_id'))
    value: Mapped[str] = mapped_column(nullable=False)

    statement: Mapped["Statement"] = relationship(back_populates='references')

    def __repr__(self):
        return f'Reference #{self.id}; inside of {self.inside_id}: {self.property_id}={self.value}'


class Qualifier(Base):
    __tablename__ = 'qualifiers'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    property: Mapped[str] = mapped_column(nullable=False)
    value_type: Mapped[str] = mapped_column(nullable=False)
    value: Mapped[str] = mapped_column(nullable=False)
    statement_id: Mapped[int] = mapped_column(ForeignKey('statements.id'))
    item_id: Mapped[str] = mapped_column(ForeignKey('items.wikidata_id'))

    statement: Mapped["Statement"] = relationship(back_populates='qualifiers')


class Label(Base):
    __tablename__ = 'item_labels'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    item_id: Mapped[str] = mapped_column(ForeignKey('items.wikidata_id'))
    label: Mapped[str] = mapped_column(String, nullable=False)
    language: Mapped[str] = mapped_column(String, nullable=False)

    item: Mapped[Item] = relationship(back_populates="labels")

class ItemStatement(Base):
    __tablename__ = 'item_statements'
    item_id: Mapped[str] = mapped_column(ForeignKey('items.wikidata_id'), primary_key=True)
    statement_id: Mapped[int] = mapped_column(ForeignKey('statements.id'), primary_key=True)
