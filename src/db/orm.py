import asyncio
import logging
import sqlalchemy
from sqlalchemy import select, Sequence, insert
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
from sqlalchemy.orm import selectinload

from src.db.models import Property, Statement, Item, Reference, Label, Qualifier

from src.db.base import Base

logger = logging.getLogger(__name__)

async def get_property(session: AsyncSession, prop_id: str | None = None) -> Property | Sequence[Property]:
    if prop_id:
        prop_there = await session.execute(
            select(Property).where(Property.wikidata_id == prop_id)
        )
        return prop_there.scalars().first()
    else:
        prop_there = await session.execute(
            select(Property)
        )
        return prop_there.scalars().all()


async def add_properties_labels(session: AsyncSession, labels_list: list[dict]) -> None:
    try:
        for label_dict in labels_list:
            prop_id, labels = label_dict['prop_id'], label_dict['item_labels'][0]
            prop = await get_property(session, prop_id)
            if not prop:
                logger.warning(f"in add_properties_labels {prop_id} is not found")
                continue
            if not prop.title:
                prop.title = f'{labels}'

        await session.commit()
    except SQLAlchemyError as e:
        logger.warning(f"RolledBack while SQLAlchemyError in add_properties_labels")
        await session.rollback()


async def get_statement(session: AsyncSession, statement_id: int) -> Statement:
    statement_query = await session.execute(
        select(Statement).where(Statement.id == statement_id).options(
            selectinload(Statement.references), selectinload(Statement.qualifiers), selectinload(Statement.items)
        )
    )

    return statement_query.scalars().first()


async def get_item(session: AsyncSession, item_id: int) -> Item:
    item_query = await session.execute(
        select(Item).where(Item.wikidata_id == item_id)
    )

    return item_query.scalars().first()


async def add_property(db_pool, prop_id, prop_type, retry_count=0):
    async with db_pool() as session:
        try:
            stmt = select(Property).where(Property.wikidata_id == prop_id)
            result = await session.execute(stmt)
            property_exists = result.scalar()

            if not property_exists:
                insert_stmt = insert(Property).values(wikidata_id=prop_id, datatype=prop_type)
                await session.execute(insert_stmt)
                await session.commit()

        except (SQLAlchemyError, sqlalchemy.exc.OperationalError) as e:
            logger.warning(f"Rolled back in add_property {prop_id}")
            await session.rollback()
            if isinstance(e, SQLAlchemyError):
                return
            if not isinstance(e, SQLAlchemyError) and retry_count < 5:
                await asyncio.sleep(1)
                logger.warning(f"DB Locked in add_property while adding {prop_id}. Trying again.")
                await add_property(db_pool, prop_id, prop_type, retry_count + 1)
            else:
                raise e


async def add_statement(db_pool, prop_id, statement_value, statement_rank, item_wikidata_id, retry_count=0):
    async with db_pool() as session:
        item = await get_item(session, item_wikidata_id)
        #to_db_statement = Statement(value=statement_value, rank=statement_rank)
        #to_db_statement.property_id = prop_id
        #to_db_statement.items.append(item)
        try:
            insert_stmt = insert(Statement).values(property_id=prop_id, value=statement_value,
                                                   rank=statement_rank).returning(Statement)
            result = await session.execute(insert_stmt)
            statement_to_db = result.scalar()
            statement_to_db.items.append(item)
            await session.commit()
            return statement_to_db.id
        except (sqlalchemy.exc.OperationalError, SQLAlchemyError) as e:
            logger.warning(f"Rolled back in add_statement: {prop_id, statement_value, statement_rank, item_wikidata_id}")
            await session.rollback()
            if retry_count < 10:
                await asyncio.sleep(1)
                logger.warning(
                    f"Statement addition #{retry_count + 1} was failed in add_statement: db is locked. Will "
                    f"try again.")
                await add_statement(db_pool, prop_id, statement_value, statement_rank, item_wikidata_id,
                                    retry_count + 1)
            else:
                raise e


async def add_references(db_pool, statement_id: int, references: list[Reference]):
    async with db_pool() as session:
        try:
            for ref_block in references:
                prop_id, prop_type, value, inside_id = ref_block
                refrnc = Reference(inside_id=inside_id, value=value, property_id=prop_id, statement_id=statement_id)
                session.add(refrnc)
            await session.commit()
        except SQLAlchemyError as e:
            logger.warning(f"Rolled back in add_references {statement_id, references}")
            await session.rollback()


async def add_labels(db_pool, labels: dict[dict], to_db_item_id: str, retry_count: int = 0):
    async with db_pool() as session:
        try:
            for label in labels.values():
                to_db_label = Label(label=label['value'], language=label['language'], item_id=to_db_item_id)
                session.add(to_db_label)
            await session.commit()
        except (SQLAlchemyError, sqlalchemy.exc.OperationalError) as e:
            await session.rollback()
            if isinstance(e, SQLAlchemyError):
                return
            if retry_count < 5:
                await asyncio.sleep(1)
                await add_labels(db_pool, labels, to_db_item_id, retry_count)
            else:
                raise e


async def add_qualifiers(db_pool, qualifiers, to_db_item_id: str, from_db_statement_id: int):
    async with db_pool() as session:
        try:
            for qualifier in qualifiers:
                prop_id, val_type, statement_value = qualifier
                qual_to_db = Qualifier(property=prop_id, value_type=val_type,
                                       value=statement_value, statement_id=from_db_statement_id,
                                       item_id=to_db_item_id)
                session.add(qual_to_db)
            await session.commit()
        except SQLAlchemyError as e:
            logger.warning(f"Rolled back in add_qualifiers: {qualifiers, to_db_item_id, from_db_statement_id}", e)

            await session.rollback()
