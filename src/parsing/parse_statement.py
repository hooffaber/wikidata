import logging

import asyncio
import sqlalchemy
from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError

from src.constants.config import DB_ENTITY_CREATE_TRIES_BEFORE_ERROR
from src.db.models import Item, SiteLink
from src.db.orm import (add_property, add_statement, add_references, add_properties_labels, add_labels,
                        add_qualifiers)
from src.utils.link_transformer.links_transform import convert_link
from src.utils.item_labels.list_convert import convert_list
from src.utils.getters import get_property_labels
from src.constants.statement_type import StatementType

logger = logging.getLogger(__name__)


async def parse_result(result, db_pool, tries_quantity: int = 0):
    if not result:
        return None
    try:
        item = result['data']['entities'][result['item_num']]
        item_wikidata_id = item['id']
        stmnts = item['claims']
        site_links = item['sitelinks']
        labels = item['labels']
    except KeyError as key_error:
        logger.warning("No page info in parse_result:", result)
        return None
    try:
        async with db_pool() as session:
            to_db_item = Item(wikidata_id=item_wikidata_id)
            session.add(to_db_item)
            await session.commit()
    except sqlalchemy.exc.OperationalError as e:
        if tries_quantity > DB_ENTITY_CREATE_TRIES_BEFORE_ERROR:
            raise e
        await asyncio.sleep(tries_quantity + 1)
        logger.warning(f"Item wikidata creating #{tries_quantity + 1} was failed in parse_result: db is locked. Will "
                       f"try again.")
        await parse_result(result, db_pool, tries_quantity + 1)

    results = await process_blocks(db_pool, stmnts)

    return {
        "item_wikidata_id": item_wikidata_id,
        "item_labels": labels,
        "site_links": site_links,
        "statements": [result for result in results if result]
    }


async def process_blocks(db_pool, block, statement_type=StatementType.default_statement.value):
    tasks = []
    if statement_type == StatementType.default_statement.value:
        for prop in block:
            for pair_block in block[prop]:
                tasks.append(asyncio.create_task(parse_element(pair_block, db_pool)))
    elif statement_type == StatementType.reference_statement.value:
        for i, reference in enumerate(block):
            try:
                for ref_snak in reference['snaks']:
                    try:
                        snak = reference['snaks'][ref_snak]
                        for snak_elem in snak:
                            tasks.append(asyncio.create_task(
                                parse_element(snak_elem, db_pool,
                                              StatementType.reference_statement.value, i)))
                    except KeyError as e:
                        logger.error(f"Keyerror at {reference}: {e}")
            except KeyError as e:
                logger.error(f"No snaks in reference {reference}")
    elif statement_type == StatementType.qualifier_statement.value:
        try:
            for pair_block in block:
                for val in block[pair_block]:
                    tasks.append(asyncio.create_task(parse_element(val, db_pool,
                                                                   StatementType.qualifier_statement.value)))
        except KeyError as e:
            logger.error(f"No data in qualifiers at {block}")

    results = await asyncio.gather(*tasks)

    return results


async def parse_element(block, db_pool,
                        statement_type=StatementType.default_statement.value,
                        ref_pose: int | None = None):
    try:
        if statement_type == StatementType.default_statement.value:
            snak_block = block['mainsnak']
        elif statement_type == StatementType.reference_statement.value:
            snak_block = block
        elif statement_type == StatementType.qualifier_statement.value:
            snak_block = block

        prop_id = snak_block['property']
        prop_type = snak_block['datatype']
        statement_rank = None

        if statement_type == StatementType.default_statement.value:
            statement_rank = block['rank']

        statement_value = await resolve_value(prop_id, snak_block, prop_type)

        references = []
        if 'references' in block and statement_type == StatementType.default_statement.value:
            references = await process_blocks(db_pool, block['references'],
                                              statement_type=StatementType.reference_statement.value)
        qualifiers = []
        if 'qualifiers' in block and statement_type == StatementType.default_statement.value:
            qualifiers = await process_blocks(db_pool, block['qualifiers'],
                                              statement_type=StatementType.qualifier_statement.value)

        if statement_type == StatementType.reference_statement.value:
            return prop_id, prop_type, statement_value, ref_pose
        elif statement_type == StatementType.qualifier_statement.value:
            return prop_id, prop_type, statement_value
        return prop_id, prop_type, statement_value, statement_rank, references, qualifiers

    except KeyError as e:
        logger.error(f"In parse_element keyerror at {block}: {e}")
        return None


async def resolve_value(prop_id, snak, prop_type):
    try:
        if prop_type == 'wikibase-item':
            return snak['datavalue']['value']['id']
        elif prop_type == 'external-id':
            link = await convert_link(prop_id, snak['datavalue']['value'])
            return link if link else str(snak['datavalue']['value'])
        elif prop_type == 'time':
            return 'novalue'
        else:
            return str(snak['datavalue']['value'])
    except (KeyError, Exception) as e:
        if isinstance(e, KeyError):
            logger.error(f"Keyerror at resolve_value {prop_id, snak, prop_type}")
            return 'novalue'


async def process_sitelinks(db_pool, to_db_item_id, site_links, retry_count: int = 0):
    async with db_pool() as session:
        try:
            item_statement = select(Item).where(Item.wikidata_id == to_db_item_id)
            item_query = await session.execute(item_statement)
            item = item_query.scalar()
            if not item:
                logger.error(f"No item in db while in process_sitelinks: {to_db_item_id}")
                return
            for site_link in site_links:
                site, title, badges = site_links[site_link].values()
                badges = ', '.join(badges) or None
                to_db_site_link = SiteLink(site=site, title=title, badges=badges)
                item.site_links.append(to_db_site_link)
                session.add(item)
            await session.commit()
        except sqlalchemy.exc.OperationalError as e:
            await session.rollback()
            if retry_count < DB_ENTITY_CREATE_TRIES_BEFORE_ERROR:
                await asyncio.sleep(1)
                await process_sitelinks(db_pool, to_db_item_id, site_links, retry_count + 1)
            else:
                raise Exception(f"Failed to process_sitelinks after {retry_count} retries.") from e


async def save_property_labels(db_pool, results):
    try:
        prop_ids = []
        for result in results:
            if result:
                prop_ids.append(result[0])

        chunk_size = 50
        prop_ids = list(set(prop_ids))
        prop_portions = [prop_ids[i:i + chunk_size] for i in range(0, len(prop_ids), chunk_size)]
        labels_list = []
        for portion in prop_portions:
            labels = await get_property_labels(portion, languages=['en', 'ru'])
            labels_list.append(labels)
        property_labels_list = convert_list(labels_unconverted=labels_list)
        async with db_pool() as session:
            await add_properties_labels(session=session, labels_list=property_labels_list)
    except SQLAlchemyError as e:
        logger.critical(f"SQLAlchemyError while operate_property_labels: {e}")
    except RuntimeError as e:
        logger.warning(f"RuntimeError while operate_property_labels: {e}")


async def save_statements(db_pool, results, item_wikidata_id):
    for result in results:
        if result:
            logger.debug(f"Saving statements {result} in {item_wikidata_id}")
            prop_id, prop_type, statement_value, statement_rank, references, qualifiers = result
            await add_property(db_pool, prop_id, prop_type)
            from_db_statement_id = await add_statement(db_pool, prop_id, statement_value, statement_rank,
                                                       item_wikidata_id)
            print(
                f"IN SAVE STATEMENTS FROM_DB_STMT_ID IS {from_db_statement_id} WHEN ITEM_WIKIDATA_ID IS {item_wikidata_id}")
            if qualifiers:
                logger.debug(f"Saving qualifiers {qualifiers} in {item_wikidata_id}")
                await add_qualifiers(db_pool, qualifiers, item_wikidata_id, from_db_statement_id)

            if references:
                logger.debug(f"Saving references {references} in {item_wikidata_id}")
                await add_references(db_pool, from_db_statement_id, references)


async def process_item_labels(db_pool, labels: dict[dict], to_db_item_id: str):
    await add_labels(db_pool, labels=labels, to_db_item_id=to_db_item_id)


async def save_results(db_pool, parsed):
    await process_item_labels(db_pool, parsed['item_labels'], parsed['item_wikidata_id'])
    await process_sitelinks(db_pool, parsed['item_wikidata_id'], parsed['site_links'])
    await save_statements(db_pool, parsed['statements'], parsed['item_wikidata_id'])
    await save_property_labels(db_pool, parsed['statements'])
