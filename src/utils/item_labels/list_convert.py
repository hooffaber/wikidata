from typing import Any


def convert_list(labels_unconverted: list[list[list[dict]]] | tuple[Any]):
    result = []
    for labels_list in labels_unconverted:
        for labels in labels_list:
            result.append(labels)

    return result
