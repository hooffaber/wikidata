import logging
import httpx
from httpx import Timeout

from src.constants.config import MAIN_URL

logger = logging.getLogger(__name__)


async def get_properties(property_ids: list[str] | str) -> list[dict[str]] | dict[str]:
    """
    Получает информацию (title, datatype, item_labels, descriptions, ...) по свойству property в виде json по её id (или по нескольким):
    :param property_ids: id свойства(свойств), например, "P24" или "P44|P172|P99"
    :return: Информация о свойстве(свойствах) в виде словаря(списка словарей)
    """

    if isinstance(property_ids, list):
        id_param = '|'.join(property_ids)
    else:
        id_param = property_ids

    get_params = {
        'action': 'wbgetentities',
        "format": "json",
        'ids': f"{id_param}"  # для одного - "P33", для нескольких "P21|P41|P65..."
    }

    timeout = Timeout(None)
    try:
        async with httpx.AsyncClient(timeout=timeout) as client:
            r = await client.get(MAIN_URL, params=get_params)

            if r.status_code != 200:
                raise RuntimeError(f"Error status_code={r.status_code} in get_property({id_param})")

            result_content = r.json()
            try:
                if isinstance(property_ids, list):
                    prop = result_content['entities']
                else:
                    prop = result_content['entities'][id_param]

            except KeyError as kErr:
                raise RuntimeError(f"Error in get_property({id_param}): no such property(ies):\n{kErr}")
    except httpx.RemoteProtocolError as e:
        logger.critical(f"Remote Protocol Error in get_properties while processing {property_ids}")
    return prop


async def get_property_labels(property_ids: str | list[str], languages=None):
    """
    Извлекает из информации о свойстве её названия на заданных языках
    :param property_id:
    :param languages:
    :return:
    """
    if isinstance(property_ids, str):
        prop = await get_properties(property_ids)
        if prop:
            labels = prop['labels']

            if not languages:
                return [{label: labels[label]['value']} for label in labels]

            return [{label: labels[label]['value']} for label in labels if label in languages]
        return []

    props = await get_properties(property_ids)
    results = []
    for prop_key in props:
        prop = props[prop_key]
        labels = prop['labels']

        if not languages:
            results.append({"prop_id": prop['id'], "item_labels": labels})
        else:
            labels = [{label: labels[label]['value']} for label in labels if label in languages]
            results.append({"prop_id": prop['id'], "item_labels": labels})

    return results
