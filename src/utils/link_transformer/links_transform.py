import logging
import httpx
from src.constants.config import REST_API_URL

logger = logging.getLogger(__name__)


async def get_id_formatter(property_id: str, link_value: str):
    try:
        async with httpx.AsyncClient() as client:
            r = await client.get(
                url='/'.join([REST_API_URL, 'properties', property_id])
            )

            response_data = r.json()
            try:
                template_links = response_data['statements']['P1630']
                if len(template_links) == 1:
                    template_link = template_links[0]['value']['content']
                    return template_link
                elif len(template_links) > 1:
                    template_links = ';'.join([link['value']['content'] for link in template_links])
                    return template_links
                else:
                    raise RuntimeError(f'Необработанная ситуация: попытка получить несколько шаблонов в {property_id}')

            except KeyError as e:
                raise KeyError(f"Ошибка при попытке получить шаблон ссылки: {e}")
    except httpx.RequestError as e:
        logger.debug(f"{link_value} is not a link in get_id_formatter for {property_id}")
    except httpx.HTTPStatusError as e:
        logger.error(f"Error response {e.response.status_code} while requesting {e.request.url!r}")


async def convert_link(property_id: str, link_value: str):
    try:
        template_link: str = await get_id_formatter(property_id, link_value)
        link = template_link.replace('$1', link_value)
        return link
    except (AttributeError, KeyError) as e:
        return None
