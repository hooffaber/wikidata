from enum import Enum


class StatementType(Enum):
    default_statement = 'statement'
    reference_statement = 'reference_statement'
    qualifier_statement = 'qualifier_statement'