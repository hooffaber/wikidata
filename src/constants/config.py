MAIN_URL = 'https://www.wikidata.org/w/api.php'
REST_API_URL = 'https://www.wikidata.org/w/rest.php/wikibase/v0/entities'
DB_ENTITY_CREATE_TRIES_BEFORE_ERROR = 10
